<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/login', 'UserController@index')
    ->name('login');
Route::post('/login', 'UserController@login');

Route::get('/logout', 'UserController@destroy');

Route::get('/register', 'UserController@register')
    ->name('register');
Route::post('/register', 'UserController@store');

Route::get('siswa', 'SiswaController@index');
Route::get('siswa/{id}', 'SiswaController@show')
    ->where('id', '\d+');

Route::get('siswa/{id}/delete', 'SiswaController@delete');
Route::post('siswa/{id}/delete', 'SiswaController@destroy');

Route::get('siswa/create', 'SiswaController@create');
Route::post('siswa/create', 'SiswaController@store');

Route::get('siswa/{siswa}/edit', 'SiswaController@edit')
    ->where('siswa', '\d+');
Route::post('siswa/{siswa}/edit', 'SiswaController@update');
