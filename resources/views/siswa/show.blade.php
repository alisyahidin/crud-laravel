@extends('layout.master')

@section('content')
    <h1 class="text-center my-2 pb-4">Data Siswa</h1>
    <div class="card col-4 mx-auto text-center">
        <div class="card-block">
            <div class="card-title">Name</div>
            <hr>
            <div class="card-text"><h3>{{ $name }}</h3></div>
            <hr>
            <div class="card-title">Address</div>
            <hr>
            <div class="card-text"><h3>{{ $address }}</h3></div>
        </div>
    </div>
    <div class="text-center mt-4">
        <a href="/siswa/{{ $id }}/edit" class="btn btn-primary text-center">Edit</a>
        <a href="/siswa/{{ $id }}/delete" class="btn btn-danger text-center">Delete</a>
    </div>

@endsection
