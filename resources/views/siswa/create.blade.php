@extends ('layout.master')

@section('content')
    <h1 class="text-center pb-5">Add New Siswa</h1>

    <div class="container col-6 text-center">

        <div class="card card-inverse card-info mb-3 text-center">
          <div class="card-block">
            <blockquote class="card-blockquote">

                <form method="POST" action="/siswa/create">
                    {{ csrf_field() }}
                    <div class="form-group col-11 mx-auto">
                        <label for="name"><h3>Name:</h3></label>
                        <input type="text" class="form-control" name="name" id="name">
                    </div>
                    <hr>
                    <div class="form-group col-11 mx-auto">
                        <label for="address"><h3>Address:</h3></label>
                        <input type="text" class="form-control" name="address" id="address">
                    </div>
                    <hr>
                    <button type="submit" class="btn btn-primary">Add New</button>
                </form>

            </blockquote>
          </div>
        </div>
    </div>
@endsection
