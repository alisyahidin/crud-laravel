@extends ('layout.master')

@section('content')
    <h1 class="text-center pb-5">Edit Siswa</h1>

    <div class="container col-6 text-center">

        <div class="card mb-3 text-center">
          <div class="card-block">
            <blockquote class="card-blockquote">

                <form method="POST" action="/siswa/{{ $siswa['id'] }}/edit">
                    {{ csrf_field() }}
                    <div class="form-group col-11 mx-auto">
                        <label for="name"><h4>Name:</h4></label>
                        <input type="text" class="form-control text-center" name="name" id="name" value="{{ $siswa['name'] }}">
                    </div>
                    <div class="form-group col-11 mx-auto">
                        <label for="address"><h4>Address:</h4></label>
                        <input type="text" class="form-control text-center" name="address" id="address" value="{{ $siswa['address'] }}">
                    </div>
                    <button type="submit" class="btn btn-primary">Update</button>
                </form>

            </blockquote>
          </div>
        </div>
    </div>
@endsection
