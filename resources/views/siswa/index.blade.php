@extends ('layout.master')

@section('content')
    <div class="text-center pb-5">
        <h1>Data Siswa Kelas Programmer</h1>
        <a href="/" class="btn btn-link">Welcome Screen</a>
    </div>
    <table class="table table-bordered col-12 text-center">
        <thead>
            <tr>
                <th class="text-center">No.</th>
                <th class="w-25 text-center">Nama</th>
                <th class="w-50 text-center">Alamat</th>
                <th class="text-center" colspan="2">Options</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($siswas as $siswa)
            <tr>
                <td>
                    {{ $loop->index+1 }}
                </td>
                <td><a href="/siswa/{{ $siswa['id'] }}">{{ $siswa['name'] }}</a></td>
                <td>{{ $siswa['address'] }}</td>
                <td>
                    <a class="btn btn-secondary" href="/siswa/{{ $siswa['id'] }}/edit">
                        <i class="fa fa-edit"></i>
                    </a>
                </td>
                <td>
                    <form action="{{ action('SiswaController@destroy', ['id' => $siswa['id']]) }}" method="post">
                        {{ csrf_field() }}
                        <button class="btn btn-secondary" type="submit">
                            <i class="fa fa-trash"></i>
                        </button>
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>

    <div class="text-center">
        <a href="/siswa/create" class="btn btn-primary mt-3"><i class="fa fa-plus"></i> Add New Siswa</a><br>
    </div>
@endsection
