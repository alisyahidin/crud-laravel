@extends('layout.master')

@section('content')
    <div class="card text-center mt-5 mx-auto" style="width: 40%">
        <div class="card-header">
            Delete Siswa
        </div>
        <div class="card-block">
            <p class="card-title">Are you sure to delete</p>
            <h3 class="text-center text-info">{{ $name }}</h3>
            <div class="mt-3">
                <form action="/siswa/{{ $id }}/delete" method="POST">
                    {{ csrf_field() }}
                    <button type="submit" class="btn btn-outline-danger"><i class="fa fa-check"></i></button>
                    <a href="/siswa" class="btn btn-outline-success"><i class="fa fa-close"></i></a>
                </form>
            </div>
        </div>
    </div>
@endsection
