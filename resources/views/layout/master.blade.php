<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Data Siswa</title>
    <link rel="stylesheet" href="/app.css">
    <link rel="stylesheet" href="/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="/assets/fa/css/font-awesome.min.css">
    <script src="/assets/jquery.min.js"></script>
    <script src="/assets/css/bootstrap.min.js"></script>
</head>
<body>
    @include('layout.nav')

    <div class="container pt-3">
        @yield('content')
    </div>
</body>
</html>
