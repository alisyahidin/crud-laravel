<nav class="navbar navbar-toggleable-md navbar-inverse bg-inverse">
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#menuNav" aria-controls="menuNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
    </button>
    <div class="container">
        <a class="navbar-brand text-info" href="/siswa"><i class="fa fa-home"></i> Home</a>

        <div class="collapse navbar-collapse" id="menuNav">
            @if (Auth::check())
                <a class="nav-link ml-auto text-info" href="/">{{ Auth::user()->name }}</a>
                <a href="/logout" class="nav-link text-info">Logout</a>
            @else
                <a class="nav-link ml-auto text-info" href="/login">Login</a>
                <a class="nav-link text-info" href="{{ route('register') }}">Register</a>
            @endif
        </div>
    </div>
</nav>
