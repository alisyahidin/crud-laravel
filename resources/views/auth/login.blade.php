@extends('layout.master')

@section('content')
    <form class="col-4 mx-auto mt-5 text-center" action="/login" method="POST">
        {{ csrf_field() }}
        <div class="form-group">
            <label for="username">Username</label>
            <input type="text" class="form-control" name="username" id="username" placeholder="Username">
        </div>

        @if (count($errors))
            @foreach ($errors->get('username') as $error)
                <div class="alert alert-danger mt-3">
                    {{ $error }}
                </div>
            @endforeach
        @endif

        <div class="form-group">
            <label for="password">Password</label>
            <input type="password" class="form-control" name="password" id="password" placeholder="Password">
        </div>

        @if (count($errors))
            @foreach ($errors->get('password') as $error)
                <div class="alert alert-danger mt-3">
                    {{ $error }}
                </div>
            @endforeach
        @endif

        <button type="submit" class="btn btn-primary">Login</button>

        @if (count($errors))
            @foreach ($errors->get('message') as $error)
                <div class="alert alert-danger mt-3">
                    {{ $error }}
                </div>
            @endforeach
        @endif
    </form>
@endsection
