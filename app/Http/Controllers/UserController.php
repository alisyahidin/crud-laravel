<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest')->except('destroy');
    }
    public function index()
    {
        return view('auth.login');
    }
    public function register()
    {
        return view('auth.register');
    }

    public function login()
    {
        $this->validate(request(), [
            'username' => 'required',
            'password' => 'required'
        ]);

        if (auth()->attempt(request(['username', 'password']))) {
            return redirect('/siswa');
        }
        return back()->withErrors(['message' => 'username or password not match']);
    }
    public function store()
    {
        $this->validate(request(), [
            'name' => 'required',
            'username' => 'required',
            'password' => 'required'
        ]);

        $user = User::create([
            'name' => request('name'),
            'username' => request('username'),
            'password' => bcrypt(request('password'))
        ]);

        auth()->login($user);

        return redirect('/siswa');
    }
    public function destroy()
    {
        auth()->logout();

        return redirect('/siswa');
    }
}
